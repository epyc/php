# PHP (an EPYC Docker image)

## Branches

- 7.0
- 7.0-xdebug
- 7.1
- 7.1-xdebug

## Extensions and tools

- nano
- wget
- git
- mysql
- soap
- zip/unzip
- gd
- composer
- xdebug (only on *-xdebug)